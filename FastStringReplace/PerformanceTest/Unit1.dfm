object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'StringReplace() Test'
  ClientHeight = 757
  ClientWidth = 776
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 48
    Width = 48
    Height = 13
    Caption = 'The sting:'
  end
  object Label2: TLabel
    Left = 16
    Top = 151
    Width = 76
    Height = 13
    Caption = 'Search pattern:'
  end
  object Label3: TLabel
    Left = 16
    Top = 197
    Width = 65
    Height = 13
    Caption = 'Replace with:'
  end
  object Label4: TLabel
    Left = 16
    Top = 244
    Width = 102
    Height = 13
    Caption = 'Number of iterations:'
  end
  object Label5: TLabel
    Left = 16
    Top = 12
    Width = 353
    Height = 23
    Caption = 'This is a StringReplace() performance test'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Button1: TButton
    Left = 168
    Top = 257
    Width = 113
    Height = 25
    Caption = 'Run the tests!'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Memo1: TMemo
    Left = 16
    Top = 288
    Width = 745
    Height = 446
    TabOrder = 1
  end
  object Memo2: TMemo
    Left = 16
    Top = 64
    Width = 745
    Height = 81
    Lines.Strings = (
      
        'alksj sldkj slkfj woeiurlkk nxcvlckjiuwr lkjwel '#225' v ,ssdnfklwejr' +
        'owu werfwesd alksj sldkj slkfj woeiurlkk nxcvlckjiuwr lkjwel v ,' +
        'ssdnfklwejrowu werfwesd '
      
        ',ssdnfklwejrowu werfwesd alksj sldkj slkfj woeiurlkk nxcvlckjiuw' +
        'r lkjwel v ,ssdnfklwejrowu werfwesd')
    TabOrder = 2
  end
  object Edit1: TEdit
    Left = 16
    Top = 168
    Width = 745
    Height = 21
    TabOrder = 3
    Text = 's'
  end
  object Edit2: TEdit
    Left = 16
    Top = 214
    Width = 745
    Height = 21
    TabOrder = 4
    Text = 'abcd'
  end
  object Edit3: TEdit
    Left = 16
    Top = 261
    Width = 113
    Height = 21
    TabOrder = 5
    Text = '100000'
  end
  object Button2: TButton
    Left = 408
    Top = 7
    Width = 75
    Height = 25
    Caption = 'Case 1'
    TabOrder = 6
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 504
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Case 2'
    TabOrder = 7
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 599
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Case 3'
    TabOrder = 8
    OnClick = Button4Click
  end
  object Button5: TButton
    Left = 408
    Top = 35
    Width = 75
    Height = 25
    Caption = 'Case 4'
    TabOrder = 9
    OnClick = Button5Click
  end
  object Button6: TButton
    Left = 504
    Top = 35
    Width = 75
    Height = 25
    Caption = 'Case 5'
    TabOrder = 10
    OnClick = Button6Click
  end
  object Button7: TButton
    Left = 600
    Top = 35
    Width = 75
    Height = 25
    Caption = 'Case 6'
    TabOrder = 11
    OnClick = Button7Click
  end
end
