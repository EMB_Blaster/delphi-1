unit Unit2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, Dialogs, StdCtrls, Contnrs, uMergeSort;

type
  TElement = class(TObject)
  private
    FKey: Integer;
    FIndex: Integer;
  public
    constructor Create(const AIndex, AKey: Integer); virtual;
    property Key: Integer read FKey;
    property Index: Integer read FIndex;
    function ToString: string; override;
  end;

  TForm2 = class(TForm)
    Memo1: TMemo;
    Button1: TButton;
    Edit1: TEdit;
    Label1: TLabel;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    Button2: TButton;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure PopulateList;
    procedure MergeSortList;
    procedure QuickSortList;
    procedure PrintList;
    procedure ClearList;
    procedure RunTest;
  public
    { Public declarations }
    H1, H2: DWORD;
    FList: TList;
    Count: Integer;
    UseObjectList: Boolean;
  end;

var
  Form2: TForm2;

implementation

{$R *.dfm}

{ TElement }

constructor TElement.Create(const AIndex, AKey: Integer);
begin
  inherited Create;
  FIndex := AIndex;
  FKey := AKey;
end;

function TElement.ToString: string;
begin
  Result := 'Element # ' + IntToStr(FIndex) + ' - Key = ' + IntToStr(FKey);
end;

{ TCompareFunc }

function CompareItems(Data1, Data2: Pointer): Integer;
var
  Obj1, Obj2: TElement;
begin
  Obj1 := TElement(Data1);
  Obj2 := TElement(Data2);
  Result := Obj1.Key - Obj2.Key
end;

{ TForm2 }

procedure TForm2.Button1Click(Sender: TObject);
begin
  Count := StrToInt(Edit1.Text);
  UseObjectList := CheckBox1.Checked;
  RunTest;
end;

const DISPERSION = 20;  // change this to increase or decrease dispersion of keys

procedure TForm2.PopulateList;
var
  i: Integer;
  obj: TElement;
begin
  ClearList;
  for i := 0 to count - 1 do begin
    obj := TElement.Create(i, Random(DISPERSION));
    FList.Add(obj);
  end;
end;

procedure TForm2.PrintList;
var
  i: Integer;
  obj: TElement;
begin
  if (Count > 1000) or not CheckBox2.Checked then
    Exit;

  Memo1.Lines.BeginUpdate;
  try
    for i := 0 to FList.Count - 1 do begin
      obj := TElement(FList[i]);
      Memo1.Lines.Add(obj.ToString);
    end;
    Memo1.Lines.Add('--------------------------');
  finally
    Memo1.Lines.EndUpdate;
  end;
end;

procedure TForm2.QuickSortList;
begin
  H1 := GetTickCount;
  FList.Sort(CompareItems);
  H2 := GetTickCount;
end;

procedure TForm2.MergeSortList;
begin
  H1 := GetTickCount;
  uMergeSort.MergeSortList(FList, CompareItems);
  H2 := GetTickCount;
end;

procedure TForm2.RunTest;
begin
  Memo1.Lines.Add('Test with ' + IntToStr(Count) + ' elements');

  PopulateList;
  MergeSortList;
  Memo1.Lines.Add('Merge Sort Time: ' + FormatFloat('0.0000 secs', (H2 - H1) / 1000));
  PrintList;

  PopulateList;
  QuickSortList;
  Memo1.Lines.Add('Quick Sort Time: ' + FormatFloat('0.0000 secs', (H2 - H1) / 1000));
  PrintList;

  Memo1.Lines.Add('');
end;

procedure TForm2.ClearList;
var
  i: Integer;
begin
  if not ((FList is TObjectList) and (TObjectList(FList).OwnsObjects)) then begin
    for i := FList.Count - 1 downto 0 do begin
      if (FList[i] <> nil) then begin
        TObject(FList[i]).Free;
      end;
    end;
  end;
  FList.Clear;
end;

procedure TForm2.FormCreate(Sender: TObject);
begin
  if UseObjectList then begin
    FList := TObjectList.Create(True);
  end else begin
    FList := TList.Create;
  end;
end;

procedure TForm2.FormDestroy(Sender: TObject);
begin
  ClearList;
  FList.Free;
end;

procedure TForm2.Button2Click(Sender: TObject);
begin
  Memo1.Lines.Clear;
end;

initialization
  Randomize();

end.
