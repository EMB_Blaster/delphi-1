unit MergeSortTestCases;

interface

uses TestFramework, Contnrs, uMergeSort;

type
  {$TYPEINFO ON}

  TTestCaseMergeSort = class(TTestCase)
  private
    FObjectList: TObjectListEx;
  private
    procedure PopulateList(count: integer; randomRange: integer);
    procedure CheckIncreasing;
    procedure Sort;
    procedure Dump;
  protected
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure Test10000;
    procedure Test0;
    procedure Test1;
  end;

  TElement = class(TObject)
  private
    FKey: Integer;
    FIndex: Integer;
  public
    constructor Create(const AIndex, AKey: Integer);
    property Key: Integer read FKey;
    property Index: Integer read FIndex;
    function ToString: string; override;
  end;

implementation

uses SysUtils, Classes, Windows;

constructor TElement.Create(const AIndex, AKey: Integer);
begin
  inherited Create;
  FIndex := AIndex;
  FKey := AKey;
end;

function TElement.ToString: string;
begin
  Result := 'Element # ' + IntToStr(FIndex) + ' - Key = ' + IntToStr(FKey);
end;

{ TCompareFunc }

function CompareItems(Data1, Data2: Pointer): Integer;
var
  Obj1, Obj2: TElement;
begin
  Obj1 := TElement(Data1);
  Obj2 := TElement(Data2);
  Result := Obj1.Key - Obj2.Key
end;

procedure TTestCaseMergeSort.PopulateList(count: integer; randomRange: integer);
var
  i: Integer;
  obj: TElement;
begin
  RandSeed := 0;
  FObjectList.Clear;
  for i := 0 to count - 1 do
  begin
    obj := TElement.Create(i, Random(randomRange));
    FObjectList.Add(obj);
  end;
end;

procedure TTestCaseMergeSort.Dump;
var
  elt: TElement;
  i: integer;
begin
  for i := 0 to FObjectList.Count - 1 do
  begin
    elt := TElement(FObjectList[i]);
    OutputDebugString(PChar(elt.ToString));
  end;
end;

{ TTestCaseMergeSort }


procedure TTestCaseMergeSort.CheckIncreasing;
var
  i: integer;
  a, b: TElement;
begin
  for i := 1 to FObjectList.Count - 1 do
  begin
    a := TElement(FObjectList[i - 1]);
    b := TElement(FObjectList[i]);
    CheckTrue(a.Key <= b.Key);

    // Check for stability.
    if a.Key = b.Key then
      CheckTrue(a.Index < b.Index, Format('Stable: %d < %d [%d, %d]', [a.Index, b.Index, a.Key, b.Key]));
  end;
end;

procedure TTestCaseMergeSort.Sort;
begin
  FObjectList.MergeSort(CompareItems);
end;

procedure TTestCaseMergeSort.Test10000;
begin
  PopulateList(10000, 10);
  Sort;
  // comment this to avoid OutputDebugString overhead
  Dump;
  CheckIncreasing;
end;

procedure TTestCaseMergeSort.Test1;
begin
  PopulateList(1, 10);
  Sort;
  CheckIncreasing;
end;

procedure TTestCaseMergeSort.Test0;
begin
  PopulateList(0, 10);
  Sort;
  CheckIncreasing;
end;

{ TTestCaseMergeSort }

procedure TTestCaseMergeSort.SetUp;
begin
  inherited;
  FObjectList := TObjectListEx.Create(True);
end;

procedure TTestCaseMergeSort.TearDown;
begin
  inherited;
  FObjectList.Free;
end;

initialization
  TestFramework.RegisterTest(TTestCaseMergeSort.Suite);
end.
