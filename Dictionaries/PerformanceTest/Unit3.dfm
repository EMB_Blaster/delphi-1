object Form3: TForm3
  Left = 0
  Top = 0
  Caption = 'Form3'
  ClientHeight = 513
  ClientWidth = 488
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object btnAddElements: TButton
    Left = 16
    Top = 12
    Width = 109
    Height = 25
    Caption = 'Run test'
    TabOrder = 0
    OnClick = btnAddElementsClick
  end
  object Button2: TButton
    Left = 179
    Top = 74
    Width = 113
    Height = 25
    Caption = 'Resize'
    TabOrder = 1
    OnClick = Button2Click
  end
  object Memo1: TMemo
    Left = 8
    Top = 120
    Width = 467
    Height = 378
    ReadOnly = True
    TabOrder = 2
  end
  object btnLoadFactor: TButton
    Left = 179
    Top = 43
    Width = 113
    Height = 25
    Caption = 'Load Factor'
    TabOrder = 3
    OnClick = btnLoadFactorClick
  end
  object btnInstanceSize: TButton
    Left = 179
    Top = 12
    Width = 113
    Height = 25
    Caption = 'Instance Size'
    TabOrder = 4
    OnClick = btnInstanceSizeClick
  end
end
